#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QDebug>
#include "myopenworkbook.h"
#include "myparsefields.h"
#include <QFileDialog>
#include <QTextStream>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    schoolsFile("db.xlsx"),
    templateDiplomaFile("template_A4.ods"),
    templateSertificateFile("template_A5.ods"),
    reportsDiplomaFolder("A4"),
    reportsSertificateFolder("A5"),
    excelBoys(NULL),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    QStringList subjects;

    subjects << "астрономии (23.01.2017)";
    subjects << "экономике (24.01.2017)";
    subjects << "истории (26.01.2017)";
    subjects << "английскому языку (28.01.2017)";
    subjects << "математике (31.01.2017)";
    subjects << "химии (02.02.2017)";
    subjects << "мировой художественной культуре (03.02.2017)";
    subjects << "информатике и ИКТ (06.02.2017)";
    subjects << "основам безопасности жизнедеятельности (08.02.2017)";
    subjects << "биологии (10.02.2017)";
    subjects << "географии (13.02.2017)";
    subjects << "обществознанию (15.02.2017)";
    subjects << "немецкому языку (17.02.2017)";
    subjects << "экологии (20.02.2017)";
    subjects << "технологии (22.02.2017)";

    QString currentDate = QDate::currentDate().toString("dd.MM.yyyy");
    int index = -1;

    foreach (QString subject, subjects)
    {
        ++index;
        QStringList subjectList = QRegularExpression("(\\d{2}.\\d{2}.\\d{4})").match(subject).capturedTexts();

        if (subjectList[0] == currentDate)
            break;
    }

    ui->comboBox_2->addItems(subjects);
    ui->comboBox_2->setCurrentIndex(index);
}

MainWindow::~MainWindow()
{
    delete ui;

    if (excelBoys)
        delete excelBoys;
}

void MainWindow::on_pushButton_clicked()
{
    loadSchools();

    QVector < QStringList > features;

    features.resize(6);
    features[0] << "ФИО" << "Фамилия имя отчество";
    features[1] << "Муниципалитет" << "Район, село" << "Город";
    features[2] << "Школа" << "Учебное заведение";
    features[3] << "Класс";
    features[4] << "Тип диплома" << "Место";
    features[5] << "Номер";

    QString fileName;

    if (excelBoys)
        fileName = excelBoys->getFileName();
    else
        fileName = QFileDialog::getOpenFileName(this, "Файл итогового протокола", QDir::currentPath(), "Microsoft Excel 2010 (*.xlsx)");

    if (fileName == "")
    {
        qDebug() << "Отмена открытия протокола.";

        return;
    }

    qDebug() << "Открывается протокол" << fileName;

    excelBoys = new MyOpenXlsx < MySchoolboy > (fileName, new MyOpenWorkbookHelper<MySchoolboy>(features));

    if (excelBoys->getAll().empty())
    {
        qDebug() << "В протоколе недостаточно полей. Попытка открыть протокол без дополнительных полей.";

        delete excelBoys;

        features.pop_back();
        features.pop_back();

        excelBoys = new MyOpenXlsx < MySchoolboy > (fileName, new MyOpenWorkbookHelper<MySchoolboy>(features));
    }

    if (excelBoys->getAll().empty())
        qDebug() << "Протокол пуст!";

    ui->pushButton->setText("Переоткрыть протокол");
    ui->listWidget->clear();
    ui->listWidget->setEnabled(true);
    ui->comboBox->setEnabled(true);
    ui->comboBox_2->setEnabled(true);
    ui->pushButton_2->setEnabled(true);
    ui->pushButton_5->setEnabled(true);
    ui->comboBox->clear();
    ui->comboBox->addItems(excelBoys->getTabNames());
}

void MainWindow::updateList(int index)
{
    if (index == -1)
        return;

    QVector < MySchoolboy > v = excelBoys->getTabData(index);

    foreach (MySchoolboy boy, v)
        new QListWidgetItem(boy.toQString(), ui->listWidget);
}

MySchool MainWindow::getSchoolByTags(QStringList tags)
{
    MyParseFields parse;

    tags.sort();

    //QSet < QString > tagsSet = tags.toSet();

    foreach (MySchool school, schools)
    {
        QStringList schoolTags = parse.getTagsForSchool(school.getSchool());

        schoolTags << parse.getTagForLocality(school.getLocality());
        schoolTags.sort();
/*
        // Попытаемся сравнить два множества. Если в итоге
        QSet < QString > schoolTagsSet = schoolTags.toSet();
        QSet < QString > subtract1 = schoolTagsSet.subtract(tagsSet);
        QSet < QString > subtract2 = tagsSet.subtract(schoolTagsSet);
        */

        if (tags == schoolTags)
            return school;
    }

    return MySchool();
}

MySchool MainWindow::getSchoolForData(QString schoolName, QString locality)
{
    MyParseFields parse;

    QStringList tagsForSchool = parse.getTagsForSchool(schoolName);
    locality = parse.getTagForLocality(locality);
    QStringList localityVariants;

    localityVariants << locality;
    localityVariants << QString("город ") + locality;
    localityVariants << QString("ЗАТО ") + locality;
    localityVariants << locality + QString(" район");

    foreach (QString variant, localityVariants)
    {
        QStringList tags = tagsForSchool;

        tags << variant;
        tags.sort();

        foreach (MySchool school, schools)
        {
            QStringList schoolTags = parse.getTagsForSchool(school.getSchool());

            schoolTags << parse.getTagForLocality(school.getLocality());
            schoolTags.sort();

            if (tags == schoolTags)
                return school;
        }
    }

    return MySchool();
}

MySchool MainWindow::getSchoolForBoy(MySchoolboy boy)
{
    return getSchoolForData(boy.getSchool(), boy.getLocality());
    /*
    MyParseFields parse;

    QStringList tagsForSchool = parse.getTagsForSchool(boy.getSchool());
    QString locality = parse.getTagForLocality(boy.getLocality());
    QStringList localityVariants;

    localityVariants << locality;
    localityVariants << QString("город ") + locality;
    localityVariants << QString("ЗАТО ") + locality;
    localityVariants << locality + QString(" район");

    foreach (QString variant, localityVariants)
    {
        QStringList tags = tagsForSchool;

        tags << variant;
        tags.sort();

        foreach (MySchool school, schools)
        {
            QStringList schoolTags = parse.getTagsForSchool(school.getSchool());

            schoolTags << parse.getTagForLocality(school.getLocality());
            schoolTags.sort();

            if (tags == schoolTags)
                return school;
        }
    }

    return MySchool();
    */
}

QMap<QString, QString> MainWindow::prepareBoyForPrint(MySchoolboy boy, const QString &tempateFile)
{
    QMap < QString , QString > result;
    MyParseFields parse;
    QStringList tags = parse.getTagsForName(boy.getName());

    result["surname"] = tags[0];
    result["middlename"] = tags[2];
    result["name"] = tags[1];
    result["level"] = boy.getLevel();

    tags = parse.getTagsForSchool(boy.getSchool());
    tags << parse.getTagForLocality(boy.getLocality());

    tags.sort();

    foreach (QString tag, tags)
        result["tags"] += "[" + tag + "] ";

    MySchool school = getSchoolForBoy(boy);//getSchoolByTags(tags);
    QStringList lines;

    if (tempateFile == templateDiplomaFile)
        lines = school.getTemplate().split("$");
    else
        lines = QString("%1$%2").arg(school.getSchool(), school.getLocality()).split("$");

    result["school"] = school.getSchool();

    while (lines.length() < 5)
        lines << "";

    for (int i = 0; i < lines.length(); ++i)
        result["line_" + QString::number(i)] = lines[i];

    QStringList subjectList = QRegularExpression("(.*)\\((\\d{2})\\.(\\d{2})\\.20(\\d{2})\\)").match(ui->comboBox_2->currentText()).capturedTexts();
    QStringList months;

    months << "января" << "февраля" << "марта" << "апреля" << "мая" << "июня" << "июля" << "августа" << "сентября" << "октября" << "ноября" << "декабря";

    if (subjectList[2][0] == '0')
        subjectList[2] = subjectList[2].remove(0, 1);

    result["subject"] = subjectList[1].simplified();
    result["day"] = subjectList[2];
    result["month"] = months[subjectList[3].toInt() - 1];
    result["year"] = subjectList[4];
    result["locality"] = school.getLocality();
    result["number"] = parse.getDiplomaNumber(boy.getField(MySchoolboy::Place), boy.getField(MySchoolboy::Number));

    return result;
}

void MainWindow::showReport(const MySchoolboy &boy)
{
    QMap < QString , QString > report =  prepareBoyForPrint(boy, templateDiplomaFile);

    ui->label->setText(report["tags"]);
    ui->label_2->setText(QString("Школа: \"%1\" (из %2)").arg(report["school"], QString::number(schools.size())));
    ui->lineEdit->setText(report["surname"]);
    ui->lineEdit_2->setText(report["name"]);
    ui->lineEdit_3->setText(report["middlename"]);
    ui->lineEdit_4->setText(report["level"]);
    ui->lineEdit_5->setText(report["line_0"]);
    ui->lineEdit_6->setText(report["line_1"]);
    ui->lineEdit_7->setText(report["line_2"]);
    ui->lineEdit_8->setText(report["line_3"]);
    ui->lineEdit_9->setText(report["line_4"]);
    ui->lineEdit_10->setText(report["day"]);
    ui->lineEdit_11->setText(report["month"]);
    ui->lineEdit_12->setText(report["year"]);
    ui->lineEdit_13->setText(report["subject"]);
    ui->label_13->setText(QString("Диплом %1 по:").arg(report["number"]));
}

void MainWindow::loadSchools()
{
    QVector < QStringList > features;

    features.resize(3);
    features[0] << "Название" << "Сокращенное название";
    features[1] << "Муниципалитет" << "Район, село" << "Город";
    features[2] << "Шаблон";

    schools = MyOpenXlsx < MySchool >(QDir::currentPath() + "/" + schoolsFile, new MyOpenWorkbookHelper<MySchool>(features)).getTabData(0);

    return;
    qDebug() << "Самотестирование загруженного списка школ.";

    foreach (MySchool school, schools)
    {
        MySchool result = getSchoolForData(school.getSchool(), school.getLocality());

        if (school.getSchool() != result.getSchool() || school.getLocality() != result.getLocality())
        {
            qDebug() << "Неверное распознавание:";
            qDebug() << "\tОжидалось: " << school.getSchool() << ", " << school.getLocality();
            qDebug() << "\tРезультат: " << result.getSchool() << ", " << result.getLocality();
        }

    }

    qDebug() << "Самотестирование завершено.";
}

void MainWindow::prepareReportsFolder(const QString &reportsFolder)
{
    QDir dir;

    if (dir.cd(reportsFolder))
        dir.removeRecursively();

    dir = QDir();
    dir.mkdir(reportsFolder);
    dir.cd(reportsFolder);
}

void MainWindow::processAllBoysForPrint(const QString &templateFile, const QString &reportsFolder)
{
    QVector < MySchoolboy > boys = excelBoys->getAll();

    prepareReportsFolder(reportsFolder);

    foreach (MySchoolboy boy, boys)
        processSelectedBoyForPrint(boy, templateFile, reportsFolder);
}

void MainWindow::processSelectedBoyForPrint(const MySchoolboy &boy, const QString &templateFile, const QString &reportsFolder)
{
    QTemporaryDir tempDir;

    QString program("unzip");
    QStringList arguments;

    arguments << templateFile;
    arguments << "-d";
    arguments << tempDir.path();

    QProcess *process = new QProcess();

    process->execute(program, arguments);

    QMap < QString , QString > report = prepareBoyForPrint(boy, templateFile);
    QFile file(tempDir.path() + "/content.xml");

    file.open(QFile::ReadOnly);

    QString text = QTextStream(&file).readAll();

    for (QMap < QString , QString >::const_iterator it = report.constBegin(); it != report.constEnd(); ++it)
        text = text.replace("%%" + it.key() + "%%", it.value());

    file.close();
    file.open(QFile::WriteOnly | QFile::Truncate);

    QTextStream out(&file);

    out << text;
    file.close();

    QString outputFilename = QString("%1 %2 %3.ods").arg(report["surname"], report["name"], report["middlename"]);
    QString outputFile;

    if (boy.getField(MySchoolboy::Place) == "")
    {
        outputFile = QString("%1/%2/%4").arg(QDir::currentPath(), reportsFolder, outputFilename);
    }
    else
    {
        QDir dir(QString("%1/%2/%3").arg(QDir::currentPath(), reportsFolder, boy.getField(MySchoolboy::Place)));

        qDebug() << "dir" << dir.exists() << dir.filePath("q");

        if (!dir.exists())
            dir.mkpath(".");

        outputFile = dir.filePath(outputFilename);
    }

    process->execute("sh", QStringList() << "-c" << "cd " + tempDir.path() + " && zip -r \"" + outputFile + "\" .");

    delete process;
}

void MainWindow::on_comboBox_currentIndexChanged(int index)
{
    ui->listWidget->clear();
    updateList(index);
}

void MainWindow::on_listWidget_currentRowChanged(int currentRow)
{
    MySchoolboy boy;

    if (currentRow != -1)
        boy = excelBoys->getBoyFromTabData(ui->comboBox->currentIndex(), currentRow);

    showReport(boy);
}

void MainWindow::on_pushButton_3_clicked()
{
    loadSchools();
}

// Диплом
void MainWindow::on_pushButton_2_clicked()
{
    processAllBoysForPrint(templateDiplomaFile, reportsDiplomaFolder);
}

void MainWindow::on_pushButton_4_clicked()
{
    QFile file(QDir::currentPath() + "/debug.txt");
    QVector < MySchoolboy > boys = excelBoys->getAll();//excelBoys->getTabData(ui->comboBox->currentIndex());

    file.open(QFile::WriteOnly | QFile::Truncate);

    QTextStream stream(&file);

    foreach (MySchoolboy boy, boys)
    {
        QMap < QString , QString > report = prepareBoyForPrint(boy, templateDiplomaFile);

        stream << report["surname"] << " " << report["name"] << " " << report["middlename"] << "\t"
            << report["locality"] << "\t"
            << report["school"] << "\t"
            << report["level"] << "\n";
    }

    stream << "\n---\n";

    MyParseFields parse;

    foreach (MySchool school, schools)
    {
        stream << "1. " << school.getSchool() << "\n2. " << school.getLocality() << "\n3. " << school.getTemplate() << "\n4. ";
        QStringList items = parse.getTagsForSchool(school.getSchool()) << parse.getTagForLocality(school.getLocality());
        items.sort();

        foreach (QString item, items)
        {
            stream << "[" << item << "] ";
        }

        stream << "\n\n";
    }

    foreach (MySchoolboy boy, boys)
    {
        QMap < QString , QString > report = prepareBoyForPrint(boy, templateDiplomaFile);

        stream
                << report["number"] << "\t"
                << report["subject"] << "\t"
                << report["surname"] << " " << report["name"] << " " << report["middlename"] << "\t"
                << report["school"] << ", " << report["locality"] << ", " << report["level"] << QString(" кл.") << "\t" << QString("медаль") << "\n";
    }

    file.close();
}

void MainWindow::on_pushButton_5_clicked()
{
    processAllBoysForPrint(templateSertificateFile, reportsSertificateFolder);
}
